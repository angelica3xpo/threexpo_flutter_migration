import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: const MyHomePage(),
      theme: _buildShrineTheme(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.menu),
        title: const Text('3XPO'),
        actions: const [
          Icon(Icons.favorite),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.search),
          ),
          Icon(Icons.more_vert),
        ],
      ),
    body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FloatingActionButton(
              onPressed: () {
                // Respond to button press
              },
              child: const Icon(Icons.add),
            ),
            const SizedBox(height: 8.0),
            FloatingActionButton.extended(
              onPressed: () {
                // Respond to button press
              },
              icon: const Icon(Icons.add),
              label: const Text('EXTENDED'),
            ),
            ElevatedButton.icon(
              onPressed: () {
                  // Respond to button press
              },
              icon: const Icon(Icons.add, size: 18),
              label: const Text("CONTAINED BUTTON"),
            )
          ],
        ),
      ),
    );
  }
}
// the theme automatically uses the color of the primary color as background color of the buttons
ThemeData _buildShrineTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: _shrineColorScheme,
    primaryColor: variantPrimary,
    scaffoldBackgroundColor: variantBackgroundWhite,
    cardColor: variantBackgroundWhite,
    textSelectionTheme: const TextSelectionThemeData(selectionColor: variantPrimary),
    errorColor: variantError,
    buttonTheme: const ButtonThemeData(
      colorScheme: _shrineColorScheme,
      textTheme: ButtonTextTheme.normal,
    ),
    primaryIconTheme: _customIconTheme(base.iconTheme),
    textTheme: _buildShrineTextTheme(base.textTheme),
    primaryTextTheme: _buildShrineTextTheme(base.primaryTextTheme),
    iconTheme: _customIconTheme(base.iconTheme),
  );
}

IconThemeData _customIconTheme(IconThemeData original) {
  return original.copyWith(color: textPrimary);
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
        caption: base.caption?.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          letterSpacing: defaultLetterSpacing,
        ),
        button: base.button?.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 14,
          letterSpacing: defaultLetterSpacing,
        ),
      )
      .apply(
        fontFamily: 'Rubik',
        displayColor: textPrimary,
        bodyColor: textPrimary,
      );
}

const ColorScheme _shrineColorScheme = ColorScheme(
  primary: variantPrimary,
  secondary: variantSecondary,
  surface: variantSurfaceWhite,
  background: variantBackgroundWhite,
  error: variantError,
  onPrimary: textPrimary,
  onSecondary: textPrimary,
  onSurface: textPrimary,
  onBackground: textPrimary,
  onError: variantSurfaceWhite,
  brightness: Brightness.light,
);

const Color variantSecondary = Color.fromARGB(255, 255, 255, 255); //secondary
const Color variantPrimary = Color.fromARGB(255, 255, 255, 255); //primary color

const Color textPrimary = Color.fromARGB(255, 10, 10, 10); //typography and iconography.

const Color variantError = Color(0xFFC5032B); //errors

const Color variantSurfaceWhite = Color(0xFFFFFBFA);
const Color variantBackgroundWhite = Colors.white;

const defaultLetterSpacing = 0.03;


